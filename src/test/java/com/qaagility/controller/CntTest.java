package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CntTest {
    @Test
    public void testD() {
        Cnt cnt = new Cnt();
        assertEquals(2, cnt.d(4, 2));
        assertEquals(0, cnt.d(0, 1));
        assertEquals(Integer.MAX_VALUE, cnt.d(1, 0));
    }
}

